SWEP.Base = "weapon_swtf_base"
SWEP.Category = "Team Fortress 2"

SWEP.PrintName = "SPY BASE"
SWEP.Spawnable = false

DEFINE_BASECLASS(SWEP.Base)

SWEP.ViewModel = Model"models/weapons/c_models/c_spy_arms.mdl"

SWEP.VMAnimTranslations = {}
function SWEP:GetDeploySequence()
    return self:LookupSequence("draw")
end

function SWEP:GetIdleSequence()
    return self:LookupSequence("idle")
end

function SWEP:GetShootSequence()
    return self:LookupSequence("fire")
end

function SWEP:GetReloadSequence()
    return self:LookupSequence("reload")
end
