SWEP.Base = "weapon_swtf_base"
SWEP.Category = "Team Fortress 2"

SWEP.PrintName = "ENGINEER BASE"
SWEP.Spawnable = false

DEFINE_BASECLASS(SWEP.Base)

SWEP.ViewModel = Model"models/weapons/c_models/c_engineer_arms.mdl"

SWEP.VMAnimTranslations = {}
function SWEP:GetDeploySequence()
    return self:LookupSequence("pstl_draw")
end

function SWEP:GetIdleSequence()
    return self:LookupSequence("pstl_idle")
end

function SWEP:GetShootSequence()
    return self:LookupSequence("pstl_fire")
end

function SWEP:GetReloadSequence()
    return self:LookupSequence("pstl_reload")
end
