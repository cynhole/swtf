AddCSLuaFile()

function SWEP:SetWeaponSequence(idealSequence, flPlaybackRate)
    if idealSequence == -1 then return false end
    flPlaybackRate = isnumber(flPlaybackRate) and flPlaybackRate or 1

    self:SendViewModelMatchingSequence(idealSequence)

    local owner = self:GetOwner()
    if owner:IsValid() then
        local vm = owner:GetViewModel()
        if vm:IsValid() then
            vm:SendViewModelMatchingSequence(idealSequence)
            vm:SetPlaybackRate(flPlaybackRate)
        end
    end

    -- Set the next time the weapon will idle
    self:SetWeaponIdleTime(CurTime() + (self:SequenceDuration(idealSequence) * flPlaybackRate))
    return true
end
function SWEP:SetWeaponAnim(idealAct, flPlaybackRate)
    local idealSequence = self:SelectWeightedSequence(idealAct)
    if idealSequence == -1 then return false end
    flPlaybackRate = isnumber(flPlaybackRate) and flPlaybackRate or 1

    self:SendWeaponAnim(idealAct)
    self:SendViewModelMatchingSequence(idealSequence)

    local owner = self:GetOwner()
    if owner:IsValid() then
        local vm = owner:GetViewModel()
        if vm:IsValid() then
            vm:SendViewModelMatchingSequence(idealSequence)
            vm:SetPlaybackRate(flPlaybackRate)
        end
    end

    -- Set the next time the weapon will idle
    self:SetWeaponIdleTime(CurTime() + (self:SequenceDuration() * flPlaybackRate))
    return true
end

SWEP.VMAnimTranslations = {}
function SWEP:GetDeploySequence()
    return self:LookupSequence("p_draw")
end

function SWEP:GetIdleSequence()
    return self:LookupSequence("p_idle")
end

function SWEP:GetShootSequence()
    return self:LookupSequence("p_fire")
end

function SWEP:GetReloadSequence()
    return self:LookupSequence("p_reload")
end

function SWEP:HasWeaponIdleTimeElapsed()
    return self:GetWeaponIdleTime() <= CurTime()
end

function SWEP:WeaponIdle()
    if self:HasWeaponIdleTimeElapsed() then
        self:SetWeaponSequence(self:GetIdleSequence(), 1)
    end
end
