AddCSLuaFile()

SWEP.Category = "Team Fortress 2"
SWEP.IsSWTFWeapon = true
SWEP.UseHands = false
SWEP.ScriptedEntityType = "tf_weapon"

SWEP.SwayScale = 0

DEFINE_BASECLASS(SWEP.Base)
SWEP.Primary.ClipSize = 18
SWEP.Primary.DefaultClip = 18

include "sh_definition.lua"
include "sh_animations.lua"

SWEP.ViewModel  = Model"models/weapons/c_models/c_scout_arms.mdl"

SWEP.ViewModels = { -- these will be bone merged onto the viewmodel
    --[Model"models/weapons/c_models/c_scattergun.mdl"] = true,
    --[Model"models/weapons/c_models/c_pistol/c_pistol.mdl"] = true,
    --[Model"models/weapons/c_models/c_pistol/c_pistol_festivizer.mdl"] = true,
}

-- static data in encrypted .ctx files
SWEP.m_tFileWeaponInfo = {}
SWEP.m_strWeaponData = [[ "WeaponData"
{
    // Attributes Base.
    "printname"		"#TF_Weapon_SniperRifle"
    "BuiltRightHanded"	"0"
    "weight"		"3"
    "WeaponType"		"primary"
    "ITEM_FLAG_NOITEMPICKUP" 	"1"
    "HasTeamSkins_Viewmodel"			"1"	
    
    // Attributes TF.
    "Damage"		"4"
    "Range"			"8192"
    "BulletsPerShot"	"1"
    "Spread"		"0.0"
    "TimeFireDelay"		"1.5"
    "ProjectileType"	"projectile_bullet"
    
    "DoInstantEjectBrass"	"0"
    "BrassModel"		"models/weapons/shells/shell_sniperrifle.mdl"

    // Ammo & Clip.
    "primary_ammo"		"TF_AMMO_PRIMARY"
    "secondary_ammo"	"None"
    "clip_size"		"-1"
    "default_clip"		"75"

    // Buckets.
    "bucket"		"0"
    "bucket_position"	"0"


    // Muzzleflash
    "MuzzleFlashParticleEffect" "muzzle_sniperrifle"

    // Animation.
    //"viewmodel"     -viewmodel is now defined in _items_main.txt
    //"playermodel"   -playermodel is now defined in _items_main.txt
    "anim_prefix"		"shotgun"

    // Sounds.
    // Max of 16 per category (ie. max 16 "single_shot" sounds).
    SoundData
    {
        "single_shot"		"Weapon_SniperRifle.Single"
        "reload"		"Weapon_SniperRifle.WorldReload"
        "empty"			"Weapon_SniperRifle.ClipEmpty"	
        "burst"			"Weapon_SniperRifle.SingleCrit"
    }

    // Weapon Sprite data is loaded by the Client DLL.
    TextureData
    {
        "weapon"
        {
                "file"		"sprites/bucket_sniper"
                "x"		"0"
                "y"		"0"
                "width"		"200"
                "height"		"128"
        }
        "weapon_s"
        {	
                "file"		"sprites/bucket_sniper"
                "x"		"0"
                "y"		"0"
                "width"		"200"
                "height"		"128"
        }
        "ammo"
        {
                "file"		"sprites/a_icons1"
                "x"		"55"
                "y"		"60"
                "width"		"73"
                "height"	"15"
        }
        "crosshair"
        {
                "file"		"sprites/crosshairs"
                "x"			"64"
                "y"			"0"
                "width"		"32"
                "height"	"32"
        }
        "autoaim"
        {
                "file"		"sprites/crosshairs"
                "x"			"0"
                "y"			"48"
                "width"		"24"
                "height"	"24"
        }
    }
}
]]

TF_RELOAD_START = 0
TF_RELOADING = 1
TF_RELOADING_CONTINUE = 2
TF_RELOAD_FINISH = 3

function SWEP:SetupDataTables()
    self:NetworkVar("Float", 0, "WeaponIdleTime")
    self:NetworkVar("Float", 0, "FinishReloadTime")

    self:NetworkVar("Bool", 0, "InReload")

    self:NetworkVar("Int", 0, "ShotsFired")
    self:NetworkVar("Int", 1, "WeaponMode")
end

function SWEP:GetPlayerOwner()
    local owner = self:GetOwner()
    if not (owner:IsValid() and owner:IsPlayer()) then return false end

    return owner
end

SWEP.HoldType = "pistol"
function SWEP:Deploy()
    self:SetHoldType(self.HoldType)

    local owner = self:GetPlayerOwner()
    if owner then
        local vm = owner:GetViewModel(self:ViewModelIndex())
        if vm:IsValid() then
            self:SetWeaponSequence(self:GetDeploySequence(), self:GetDeploySpeed())
            vm:SetPlaybackRate(self:GetDeploySpeed())

            local oPrim = self:GetNextPrimaryFire()
            local oSec = self:GetNextSecondaryFire()
            self:SetNextPrimaryFire(oPrim < self:GetWeaponIdleTime() and self:GetWeaponIdleTime() or oPrim)
            self:SetNextSecondaryFire(oSec < self:GetWeaponIdleTime() and self:GetWeaponIdleTime() or oSec)
        end
    end

    return true
end

function SWEP:Initialize()
    self:InitializeFileWeaponInfo()

    local WeaponInfo = self.m_tFileWeaponInfo

    self.Primary.ClipSize = WeaponInfo.iMaxClip1
    self.Primary.DefaultClip = WeaponInfo.iDefaultClip1

    self:SetClip1(self.Primary.DefaultClip)

    self:SetDeploySpeed(1)

    -- add bone merged model to viewmodel pls :)
    if self.m_tItemData and self.m_tItemData.model_player then
        self.ViewModels[self.m_tItemData.model_player] = true
    end
end

function SWEP:CanPrimaryAttack()
    if self:Clip1() <= 0 then return false end

    return true
end

function SWEP:ApplyFireDelay(flDelay)
    local flDelayMult = self:GetAttribute("mult_postfiredelay", 1)

    -- TODO: implement kill combos
    local flComboBoost = self:GetAttribute("kill_combo_fire_rate_boost", 0)
    --flComboBoost = flComboBoost * self:GetKillComboCount()

    flDelayMult = flDelayMult - flComboBoost

    -- TODO: implement runes ???
    -- Haste Powerup Rune adds multiplier to fire delay time
    --[[
	CTFPlayer *pPlayer = ToTFPlayer( GetPlayerOwner() );
	if ( pPlayer && pPlayer->m_Shared.GetCarryingRuneType() == RUNE_HASTE )
	{
		flDelayMult *= 0.5f;
	}
	else if ( pPlayer && ( pPlayer->m_Shared.GetCarryingRuneType() == RUNE_KING || pPlayer->m_Shared.InCond( TF_COND_KING_BUFFED ) ) )
	{
		flDelayMult *= 0.75f;
	}
    ]]

    return flDelay * flDelayMult
end

function SWEP:PrimaryAttack()
    --[[
        float flUberChargeAmmoPerShot = UberChargeAmmoPerShot();
        if ( flUberChargeAmmoPerShot > 0.0f )
        {
            if ( !HasPrimaryAmmo() )
                return;
        }
    ]]

    -- Check for ammunition.
    if self:Clip1() <= 0 and self:Clip1() ~= -1 then
        return end

    -- Are we capable of firing again?
    if self:GetNextPrimaryFire() > CurTime() then
        return end

    -- Get the player owning the weapon.
    local owner = self:GetOwner()
    if not owner:IsValid() then
        return end

    --if not self:CanAttack() then
    --    return end

    local flFireDelay = self:ApplyFireDelay(self:GetWeaponData(self:GetWeaponMode()).m_flTimeFireDelay)
    -- CALL_ATTRIB_HOOK_FLOAT_ON_OTHER( pPlayer, flFireDelay, hwn_mult_postfiredelay );

    -- Some weapons change fire delay based on player's health
    local flReducedHealthBonus = self:GetAttribute("mult_postfiredelay_with_reduced_health", 1)
    --if flReducedHealthBonus ~= 1.0 then
    --    flReducedHealthBonus = RemapValClamped( pPlayer->HealthFraction(), 0.2f, 0.9f, flReducedHealthBonus, 1.0f );
    --    flFireDelay *= flReducedHealthBonus;
    --end

    --[[
        if ( pPlayer->m_Shared.InCond( TF_COND_BLASTJUMPING ) )
        {
            CALL_ATTRIB_HOOK_FLOAT( flFireDelay, rocketjump_attackrate_bonus );	
        }
        else
        {
            CALL_ATTRIB_HOOK_FLOAT( flFireDelay, mul_nonrocketjump_attackrate );
        }
    ]]

    --[[
        if ( m_iPrimaryAmmoType == TF_AMMO_METAL )
        {
            if ( GetOwner() && GetAmmoPerShot() > GetOwner()->GetAmmoCount( m_iPrimaryAmmoType ) )
            {
                WeaponSound( EMPTY );
                m_flNextPrimaryAttack = gpGlobals->curtime + flFireDelay;
                return;
            }
        }
    ]]

    -- CalcIsAttackCritical();

    --[[
    #ifndef CLIENT_DLL
        if ( pPlayer->m_Shared.IsStealthed() && ShouldRemoveInvisibilityOnPrimaryAttack() )
        {
            pPlayer->RemoveInvisibility();
        }
    #endif
    ]]

    -- Minigun has custom handling
    if ( true --[[self:GetWeaponID() ~= TF_WEAPON_MINIGUN]] ) then
        -- Set the weapon mode.
        self:SetWeaponMode(TF_WEAPON_PRIMARY_MODE)
    end

    self:SetWeaponSequence(self:GetShootSequence(), 1)
    owner:SetAnimation(PLAYER_ATTACK1)

    local pProj = self:FireProjectile(owner)
    self:ModifyProjectile(pProj)

    --[[
        if ( !UsesClipsForAmmo1() )
        {
            // Sniper rifles and such don't actually reload, so we hook reduced reload here
            float flBaseFireDelay = flFireDelay;
            CALL_ATTRIB_HOOK_FLOAT( flFireDelay, fast_reload );

            float flPlaybackRate = flFireDelay == 0.f ? 0.f : flBaseFireDelay / flFireDelay;

            if ( pPlayer->GetViewModel( 0 ) )
            {
                pPlayer->GetViewModel( 0 )->SetPlaybackRate( flPlaybackRate );
            }
            if ( pPlayer->GetViewModel( 1 ) )
            {
                pPlayer->GetViewModel( 1 )->SetPlaybackRate( flPlaybackRate );
            }
        }
    ]]

    -- Set next attack times.
    self:SetNextPrimaryFire(CurTime() + flFireDelay)

    -- Don't push out secondary attack, because our secondary fire
    -- systems are all separate from primary fire (sniper zooming, demoman pipebomb detonating, etc)
    --self:SetNextSecondaryFire(CurTime() + self:GetWeaponData(self:GetWeaponMode()).m_flTimeFireDelay)

    -- Set the idle animation times based on the sequence duration, so that we play full fire animations
    -- that last longer than the refire rate may allow.
    -- NOTE: swtf handles this internally
    --self:SetWeaponIdleTime(CurTime() + self:SequenceDuration())

    --[[
        // Check the reload mode and behave appropriately.
        if ( m_bReloadsSingly )
        {
            m_iReloadMode.Set( TF_RELOAD_START );
        }

    #ifdef STAGING_ONLY
        // Remove Cond if I attack
        if ( pPlayer->m_Shared.InCond( TF_COND_NO_COMBAT_SPEED_BOOST ) )
        {
            pPlayer->m_Shared.RemoveCond( TF_COND_NO_COMBAT_SPEED_BOOST );
        }
    #endif

        m_flLastPrimaryAttackTime = gpGlobals->curtime;

        if ( ShouldRemoveDisguiseOnPrimaryAttack() )
        {
            pPlayer->RemoveDisguise();
        }
    ]]

    if self.m_tItemData and self.m_tItemData.visuals then
        self:EmitSound(self.m_tItemData.visuals.sound_single_shot or "")
    end

    self:TakePrimaryAmmo(1)
    --self:SetNextPrimaryFire(CurTime() + 0.17)
    --self:SetNextSecondaryFire(CurTime() + 0.17)
    self:SetLastShootTime()

    self:SetShotsFired(self:GetShotsFired() + 1)
end

function SWEP:GetWeaponProjectileType()
    return self:GetWeaponData(self:GetWeaponMode()).m_iProjectile
end

function SWEP:FireProjectile(pPlayer)
    -- New behavior: allow weapons to have attributes to specify what sort of
    -- projectile they fire.
    local iProjectile = self:GetAttribute("override_projectile_type", 0)

    -- Previous default behavior: ask the weapon type for what sort of projectile
    -- to launch.
    if iProjectile == 0 then
        iProjectile = self:GetWeaponProjectileType()
    end

    local pProjectile = NULL

    -- Anyone ever hear of a factory? This is a disgrace.
    --switch( iProjectile )
    --{
    if iProjectile == swtf.enum.TF_PROJECTILE_BULLET then
        self:FireBullet(pPlayer)
        --pPlayer->DoAnimationEvent( PLAYERANIMEVENT_ATTACK_PRIMARY )
    else
        swtf.LOG("unhandled projectile type %i", iProjectile)
    end
end

function SWEP:PlayWeaponShootSound()
    local strSound = ""

    if self.m_tFileWeaponInfo and istable(self.m_tFileWeaponInfo.m_tKeyValues.sounddata) then
        if false --[[self:IsCurrentAttackACrit()]] then
            strSound = self.m_tFileWeaponInfo.m_tKeyValues.sounddata.burst or ""
        else
            strSound = self.m_tFileWeaponInfo.m_tKeyValues.sounddata.single_shot or ""
        end
    end

    self:EmitSound(strSound)
end

function SWEP:FireBullet()
    self:PlayWeaponShootSound()

    --FX_FireBullets(
    --    this,
    --    pPlayer->entindex(),
    --    pPlayer->Weapon_ShootPosition(),
    --    pPlayer->EyeAngles() + pPlayer->GetPunchAngle(),
    --    GetWeaponID(),
    --    m_iWeaponMode,
    --    CBaseEntity::GetPredictionRandomSeed( UseServerRandomSeed() ) & 255,
    --    GetWeaponSpread(),
    --    GetProjectileDamage(),
    --    IsCurrentAttackACrit() )
end

function SWEP:ModifyProjectile()
    --
end

function SWEP:CanReload()
    if self:GetInReload() then return false end
    if self:Clip1() >= self:GetMaxClip1() then return false end
    if self:GetNextPrimaryFire() > CurTime() then return false end

    local owner = self:GetOwner()
    if not owner then return false end
    if owner:GetAmmoCount(self:GetPrimaryAmmoType()) < 1 then return false end

    return true
end

function SWEP:Reload()
    local ok = self:CanReload()

    if ok then
        self:SetInReload(true)

        local iSequence = self:GetReloadSequence()
        self:SetWeaponSequence(iSequence, 1)
        if self.RELOAD then
            self:EmitSound(self.RELOAD)
        end

        self:SetShotsFired(0)
        self:SetFinishReloadTime(CurTime() + self:SequenceDuration(iSequence))
        self:SetNextPrimaryFire(CurTime() + self:SequenceDuration(iSequence))
        self:SetNextSecondaryFire(CurTime() + self:SequenceDuration(iSequence))

        self:GetOwner():DoReloadEvent()

        self:OnStartReload()
    else
        self:OnReloadFail()
    end
end

function SWEP:OnStartReload()
    --
end

function SWEP:OnReloadFail()
    --
end
function SWEP:OnFinishReload()
    --
end

function SWEP:GetWeaponSpread()
    local fSpread = self:GetWeaponData(self:GetWeaponMode()).m_flSpread
    fSpread = self:GetAttribute("mult_spread_scale", fSpread)

    local owner = self:GetOwner()
    if owner:IsValid() then
        -- Some weapons change fire delay based on player's health
        local flReducedHealthBonus = self:GetAttribute("panic_attack_negative", 1.0)
        if flReducedHealthBonus ~= 1.0 then
        	--flReducedHealthBonus = RemapValClamped( pPlayer->HealthFraction(), 0.2f, 0.9f, flReducedHealthBonus, 1.0f );
        	--fSpread *= flReducedHealthBonus;
        end
    end

    return fSpread
end

function SWEP:GetIdleInterval()
    local iSeq = self:SelectWeightedSequence(ACT_VM_IDLE)
    return self:SequenceDuration(iSeq)
end

function SWEP:Think()
    local owner = self:GetOwner()

    if self:GetInReload() then
        if self:GetFinishReloadTime() > CurTime() then
            --self:InReloadThink()
        else
            -- the AE_WPN_COMPLETE_RELOAD event should handle the stocking the clip, but in case it's missing, we can do it here as well
            local j = math.min(self:GetMaxClip1() - self:Clip1(), owner:GetAmmoCount(self:GetPrimaryAmmoType()))

            -- Add them to the clip
            self:SetClip1(self:Clip1() + j)

            owner:RemoveAmmo(j, self:GetPrimaryAmmoType())

            self:SetInReload(false)
            self:OnFinishReload()
        end
    end

    self:WeaponIdle()
end

function SWEP:PrintWeaponInfo( x, y, alpha )
    --if ( self.DrawWeaponInfoBox == false ) then return end

    if (self.InfoMarkup == nil ) then
        local str
        local title_color = "<color=230,230,230,255>"
        local text_color = "<color=150,150,150,255>"

        str = "<font=HudSelectionText>"

        --if self.m_tPrefabData then
        --    str = str .. "Class:</color>\n" .. (istable(self.m_tPrefabData.used_by_classes) and table.concat(self.m_tPrefabData.used_by_classes, ",") or self.m_tPrefabData.used_by_classes) .. "</color>\n"
        --end
        --if ( self.Author != "" ) then str = str .. title_color .. "Author:</color>\t"..text_color..self.Author.."</color>\n" end
        --if ( self.Contact != "" ) then str = str .. title_color .. "Contact:</color>\t"..text_color..self.Contact.."</color>\n\n" end
        --if ( self.Purpose != "" ) then str = str .. title_color .. "Purpose:</color>\n"..text_color..self.Purpose.."</color>\n\n" end
        --if ( self.Instructions != "" ) then str = str .. title_color .. "Instructions:</color>\n"..text_color..self.Instructions.."</color>\n" end
        str = str .. "</font>"

        self.InfoMarkup = markup.Parse( str, 250 )
    end

    surface.SetDrawColor( 60, 60, 60, alpha )
    surface.SetTexture( self.SpeechBubbleLid )

    surface.DrawTexturedRect( x, y - 64 - 5, 128, 64 ) 
    draw.RoundedBox( 8, x - 5, y - 6, 260, self.InfoMarkup:GetHeight() + 18, Color( 60, 60, 60, alpha ) )

    self.InfoMarkup:Draw( x+5, y+5, nil, nil, alpha )

end
