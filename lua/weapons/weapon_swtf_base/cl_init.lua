include "shared.lua"

local viewmodel_fov = GetConVar"viewmodel_fov"
function SWEP:CalcView(owner, pos, ang, fov)
    if owner == LocalPlayer() then
        self.ViewModelFOV = viewmodel_fov:GetFloat()
    end
end

SWEP.m_tclViewModels = {}
function SWEP:ViewModelDrawn(vm)
    if self.ViewModels.BaseClass then
        self.ViewModels.BaseClass = nil
    end

    -- init, bc every gun should have one
    if not IsValid(self.m_tclViewModels[1]) then
        for mdl in next, self.ViewModels do

            local ent = ClientsideModel(mdl)
            if not IsValid(ent) then continue end

            ent:SetRenderMode(RENDERMODE_TRANSALPHA)
            ent:SetNoDraw(true)

            table.insert(self.m_tclViewModels, ent)
        end
    end

    for i, ent in ipairs(self.m_tclViewModels) do
        if ent:GetParent() ~= vm then
            ent:SetParent(vm)
            ent:SetPos(vm:GetPos())
            ent:SetAngles(vm:GetAngles())
            ent:AddEffects(EF_BONEMERGE)
            ent:AddEffects(EF_BONEMERGE_FASTCULL)
        end

        ent:DrawModel()
        render.RenderFlashlights(function()
            ent:DrawModel()
        end)
    end
end
