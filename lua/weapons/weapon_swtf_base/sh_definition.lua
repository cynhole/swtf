AddCSLuaFile()

function SWEP:InitializeFileWeaponInfo()
    self.m_tFileWeaponInfo = swtf.WeaponInfo()
    self.m_tFileWeaponInfo:Initialize(self.m_strWeaponData)
end

function SWEP:GetWeaponData(iWeaponMode)
    return self.m_tFileWeaponInfo.m_tWeaponData[iWeaponMode]
end

function SWEP:GetAttribute(strAttributeName, valDefault)
    if not self.m_tPrefabData then
        return valDefault
    end

    local tAttributes = self.m_tPrefabData.attributes

    if istable(tAttributes) then
        for _, tAttribute in next, tAttributes do
            local iAttributeID = swtf.m_AttributeManager.m_tClassToID[tAttribute.attribute_class]

            if iAttributeID == nil then
                continue
            end

            if tAttribute.attribute_class == strAttributeName then
                return tAttribute.value
            end
        end
    end

    return valDefault
end