SWEP.Base = "weapon_swtf_base"
SWEP.Category = "Team Fortress 2"

SWEP.PrintName = "SCOUT BASE"
SWEP.Spawnable = false

DEFINE_BASECLASS(SWEP.Base)

SWEP.ViewModel = Model"models/weapons/c_models/c_scout_arms.mdl"

SWEP.VMAnimTranslations = {}
function SWEP:GetDeploySequence()
    return self:LookupSequence("p_draw")
end

function SWEP:GetIdleSequence()
    return self:LookupSequence("p_idle")
end

function SWEP:GetShootSequence()
    return self:LookupSequence("p_fire")
end

function SWEP:GetReloadSequence()
    return self:LookupSequence("p_reload")
end
