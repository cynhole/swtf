SWEP.Base = "weapon_swtf_base"
SWEP.Category = "Team Fortress 2"

SWEP.PrintName = "DEMO BASE"
SWEP.Spawnable = false

DEFINE_BASECLASS(SWEP.Base)

SWEP.ViewModel = Model"models/weapons/c_models/c_demo_arms.mdl"

SWEP.VMAnimTranslations = {}
function SWEP:GetDeploySequence()
    return self:LookupSequence("g_draw")
end

function SWEP:GetIdleSequence()
    return self:LookupSequence("g_idle")
end

function SWEP:GetShootSequence()
    return self:LookupSequence("g_fire")
end

function SWEP:GetReloadSequence()
    return self:LookupSequence("g_reload_loop")
end
