--
SWEP.Base = "weapon_swtf_sniper_base"
SWEP.Category = "tf2 sniper"
SWEP.Spawnable = true

DEFINE_BASECLASS(SWEP.Base)

-- static data in encrypted .ctx files
SWEP.m_strWeaponData = [[ "WeaponData"
{
    // Attributes Base.
    "printname"		"#TF_Weapon_SniperRifle"
    "BuiltRightHanded"	"0"
    "weight"		"3"
    "WeaponType"		"primary"
    "ITEM_FLAG_NOITEMPICKUP" 	"1"
    "HasTeamSkins_Viewmodel"			"1"	
    
    // Attributes TF.
    "Damage"		"4"
    "Range"			"8192"
    "BulletsPerShot"	"1"
    "Spread"		"0.0"
    "TimeFireDelay"		"1.5"
    "ProjectileType"	"projectile_bullet"
    
    "DoInstantEjectBrass"	"0"
    "BrassModel"		"models/weapons/shells/shell_sniperrifle.mdl"

    // Ammo & Clip.
    "primary_ammo"		"TF_AMMO_PRIMARY"
    "secondary_ammo"	"None"
    "clip_size"		"-1"
    "default_clip"		"75"

    // Buckets.
    "bucket"		"0"
    "bucket_position"	"0"


    // Muzzleflash
    "MuzzleFlashParticleEffect" "muzzle_sniperrifle"

    // Animation.
    //"viewmodel"     -viewmodel is now defined in _items_main.txt
    //"playermodel"   -playermodel is now defined in _items_main.txt
    "anim_prefix"		"shotgun"

    // Sounds.
    // Max of 16 per category (ie. max 16 "single_shot" sounds).
    SoundData
    {
        "single_shot"		"Weapon_SniperRifle.Single"
        "reload"		"Weapon_SniperRifle.WorldReload"
        "empty"			"Weapon_SniperRifle.ClipEmpty"	
        "burst"			"Weapon_SniperRifle.SingleCrit"
    }

    // Weapon Sprite data is loaded by the Client DLL.
    TextureData
    {
        "weapon"
        {
                "file"		"sprites/bucket_sniper"
                "x"		"0"
                "y"		"0"
                "width"		"200"
                "height"		"128"
        }
        "weapon_s"
        {	
                "file"		"sprites/bucket_sniper"
                "x"		"0"
                "y"		"0"
                "width"		"200"
                "height"		"128"
        }
        "ammo"
        {
                "file"		"sprites/a_icons1"
                "x"		"55"
                "y"		"60"
                "width"		"73"
                "height"	"15"
        }
        "crosshair"
        {
                "file"		"sprites/crosshairs"
                "x"			"64"
                "y"			"0"
                "width"		"32"
                "height"	"32"
        }
        "autoaim"
        {
                "file"		"sprites/crosshairs"
                "x"			"0"
                "y"			"48"
                "width"		"24"
                "height"	"24"
        }
    }
}
]]

SWEP.ViewModels = {
    ["models/weapons/c_models/c_sniperrifle/c_sniperrifle.mdl"] = true,
}
function SWEP:MustBeZoomedToFire()
    local must_be_zoomed = self:GetAttribute("sniper_only_fire_zoomed", 0)

    return must_be_zoomed ~= 0
end

function SWEP:TF_Fire(owner)
    if owner:GetAmmoCount(self:GetPrimaryAmmoType()) <= 0 then
        --self:HandleFireOnEmpty()
        return
    end

    if self:MustBeZoomedToFire() then
        if not self:IsZoomed() then
            self:HandleNoScopeFireDeny()
            return
        end
    end

    if self:GetNextPrimaryFire() > CurTime() then
        return end

    self:PrimaryAttack()

    --if self:IsZoomed() then
    --    --
    --else
    --    --
    --end

    --self:SetChargedDamage(0)

    if IsValid(self.m_hSniperDot) then
        self.m_hSniperDot:ResetChargeTime()
    end
end

function SWEP:SecondaryAttack() end
function SWEP:Reload() end

function SWEP:CanAttack()
    -- more shit here :D

    return true
end

function SWEP:Think()
    local owner = self:GetOwner()

    if not owner:IsValid() then return end

    if not self:CanAttack() then
        if self:IsZoomed() then
            self:ToggleZoom()
        end

        return
    end

    --self:HandleZooms()

    if IsValid(self.m_hSniperDot) then
        self:UpdateSniperDot()
    end

    -- Start charging when we're zoomed in, and allowed to fire
    if owner.IsJumping and owner:IsJumping() then
        -- aaaaaaaa
    elseif self:GetNextSecondaryFire() <= CurTime() then
        -- charge up
    end

    -- Fire.
    if owner:KeyDown(IN_ATTACK) then
        self:TF_Fire(owner)
    end

    -- Idle.
    if not (owner:KeyDown(IN_ATTACK) or owner:KeyDown(IN_ATTACK2)) then
        -- No fire buttons down or reloading
        if --[[not self:ReloadOrSwitchWeapons() and]] self:GetInReload() == false then
            self:WeaponIdle()
        end
    end

    --BaseClass.Think(self)
end
