-- pls put your steam API key in cfg/swtf_api.cfg

local TAG = "swtf_init"
include("sh_player.lua")

swtf.m_Parser = swtf.SchemaParser()
swtf.m_AttributeManager = swtf.AttributeManager()
swtf.m_PrefabManager = swtf.PrefabManager()

function swtf.m_Parser:OnFinishParsing()
    if #swtf.m_tSchemaWaitingList > 0 then
        local ply, d = next(swtf.m_tSchemaWaitingList)
        while ply do
            net.Start(TAG)
                net.WriteUInt(swtf.enum.PARSER_VERIFY_RESPONSE, 4)

                if not d.bPlayerHasBackup or (d.strCRC == "" or d.strCRC ~= swtf.m_Parser.m_strCRC) then
                    if d.strCRC ~= "" and d.strCRC ~= swtf.m_Parser.m_strCRC then
                        swtf.LOG("%s items schema mismatch crc CL:%s vs SV:%s", ply, strCRC, swtf.m_Parser.m_strCRC)
                    end

                    -- negative response, prepare to download
                    net.WriteBool(false)

                    local compressed = swtf.m_Parser.m_strCompressedSchemaData

                    local iTotalChunks = math.ceil(#compressed / 0x7fff)

                    net.WriteUInt(iTotalChunks, 8) -- no way its more than 255 chunks lol
                    net.WriteString(swtf.m_Parser.m_strCRC)

                    swtf.LOG("sending item schema (%s, compressed: %s) in %d chunks to %s", string.NiceSize(#swtf.m_Parser.m_strRawSchemaData), string.NiceSize(#compressed), iTotalChunks, ply)
                    --net.WriteString(parser.m_strRawSchemaData)
                else
                    -- you're all good, load up
                    net.WriteBool(true)
                end
            net.Send(ply)

            ply = next(swtf.m_tSchemaWaitingList, ply)
        end

        table.Empty(swtf.m_tSchemaWaitingList)
    end
end

swtf.m_Parser:RequestSchema()

util.AddNetworkString(TAG)

swtf.m_tChunkedSchema = {}

swtf.m_tSchemaWaitingList = {}
local UndecorateNick = UndecorateNick or function(s) return s end
net.Receive(TAG, function(_, ply)
    local what = net.ReadUInt(4)

    if what == swtf.enum.PARSER_CL_VERIFY then
        swtf.LOG("%s requested item schema", ply)

        local bPlayerHasBackup = net.ReadBool()
        local strCRC = ""

        if bPlayerHasBackup then
            strCRC = net.ReadString()
        end

        if not swtf.m_Parser.m_bParsed then
            swtf.LOG("parser not ready, waiting for schema")
            swtf.m_tSchemaWaitingList[ply] = {
                bPlayerHasBackup = bPlayerHasBackup,
                strCRC = strCRC,
            }

            return
        end

        net.Start(TAG)
            net.WriteUInt(swtf.enum.PARSER_VERIFY_RESPONSE, 4)

            if not bPlayerHasBackup or (strCRC == "" or strCRC ~= swtf.m_Parser.m_strCRC) then
                if strCRC ~= "" and strCRC ~= swtf.m_Parser.m_strCRC then
                    swtf.LOG("%s items schema mismatch crc CL:%s vs SV:%s", ply, strCRC, swtf.m_Parser.m_strCRC)
                end

                -- negative response, prepare to download
                net.WriteBool(false)

                local compressed = swtf.m_Parser.m_strCompressedSchemaData

                local iTotalChunks = math.ceil(#compressed / 0x7fff)

                net.WriteUInt(iTotalChunks, 8) -- no way its more than 255 chunks lol
                net.WriteString(swtf.m_Parser.m_strCRC)

                swtf.LOG("sending item schema (%s, compressed: %s) in %d chunks to %s", string.NiceSize(#swtf.m_Parser.m_strRawSchemaData), string.NiceSize(#compressed), iTotalChunks, ply)
                --net.WriteString(parser.m_strRawSchemaData)
            else
                -- you're all good, load up
                net.WriteBool(true)
            end
        net.Send(ply)
    elseif what == swtf.enum.PARSER_RECEIVE_CHUNK then
        local iLastChunk = net.ReadUInt(8)

        -- init chunks!!
        if table.IsEmpty(swtf.m_tChunkedSchema) then
            for i = 0, math.huge do
                local chunk = string.sub(swtf.m_Parser.m_strCompressedSchemaData, (i * 0x7fff) + 1, (i + 1) * 0x7fff)
                if #chunk == 0 then break end

                swtf.m_tChunkedSchema[i + 1] = chunk
            end
        end

        if iLastChunk >= #swtf.m_tChunkedSchema then
            -- done! now fuck off
            return
        end

        local chunk = swtf.m_tChunkedSchema[iLastChunk + 1]

        net.Start(TAG)
            net.WriteUInt(swtf.enum.PARSER_RECEIVE_CHUNK, 4)

            net.WriteUInt(iLastChunk + 1, 8)

            net.WriteUInt(#chunk, 15)
            net.WriteData(chunk, #chunk)
        net.Send(ply)
    end
end)

--[[
    if not bPlayerHasBackup or strCRC ~= parser.m_strCRC then
        if strCRC ~= parser.m_strCRC then
            swtf.LOG("%s items schema mismatch crc CL:%s vs SV:%s", ply, strCRC, parser.m_strCRC)
        end

        swtf.LOG("sending item schema to %s", ply)

        -- send schema from server
    end
]]
