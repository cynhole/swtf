local TAG = "swtf_init"

swtf.m_Parser = swtf.SchemaParser()
swtf.m_AttributeManager = swtf.AttributeManager()
swtf.m_PrefabManager = swtf.PrefabManager()

hook.Add("StartCommand", "swtf.request_schema", function(ply, cmd)
    if not cmd:IsForced() then
        hook.Remove("StartCommand", "swtf.request_schema")
        swtf.m_Parser:RequestSchema()
    end
end)

swtf.m_iTotalChunks = -1
swtf.m_tChunkedSchema = {}
swtf.m_strServerCRC = ""

net.Receive(TAG, function()
    local what = net.ReadUInt(4)
    if what == swtf.enum.PARSER_VERIFY_RESPONSE then
        local bMatch = net.ReadBool()

        -- we match server's schema, load using our copy
        if bMatch then
            swtf.LOG("server reports match, loading from cache!")
            file.AsyncRead("swtf_items_game.dat", "DATA", function(_, _, status, data)
                --if status ~= FSASYNC_OK then return end

                swtf.m_Parser.m_strRawSchemaData = data
                swtf.m_Parser:ParseSchema()
            end, false)
        else
            swtf.m_iTotalChunks = net.ReadUInt(8)
            swtf.m_strServerCRC = net.ReadString()

            swtf.LOG("receiving schema from server... %d chunks", swtf.m_iTotalChunks)

            -- kick it off
            net.Start(TAG)
                net.WriteUInt(swtf.enum.PARSER_RECEIVE_CHUNK, 4)
                net.WriteUInt(0, 8)
            net.SendToServer()
        end
    elseif what == swtf.enum.PARSER_RECEIVE_CHUNK then
        local iChunk = net.ReadUInt(8)

        local iChunkLength = net.ReadUInt(15)
        local strChunkData = net.ReadData(iChunkLength)

        swtf.m_tChunkedSchema[iChunk] = strChunkData
        swtf.LOG("received chunk %d/%d", iChunk, swtf.m_iTotalChunks)

        -- done!
        if iChunk >= swtf.m_iTotalChunks then
            swtf.LOG("finished receiving schema from server, verifying!")

            local strCompressedSchema = table.concat(swtf.m_tChunkedSchema)
            local strSchema = util.Decompress(strCompressedSchema)

            local strCRC = util.CRC(strSchema)

            if strCRC == swtf.m_strServerCRC then
                swtf.LOG("received item schema verified, parsing!")
                swtf.m_Parser.m_strRawSchemaData = strSchema
                swtf.m_Parser:ParseSchema()
            else
                swtf.LOG("received item schema failed verification, what now!?!?")
                --
            end
        else
            -- hey i just received this chunk, gimme another
            net.Start(TAG)
                net.WriteUInt(swtf.enum.PARSER_RECEIVE_CHUNK, 4)

                net.WriteUInt(iChunk, 8)
            net.SendToServer()
        end
    end
end)