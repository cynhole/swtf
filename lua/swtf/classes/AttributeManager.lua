AddCSLuaFile()

local META = {
    m_tList = {},
    m_tClassToID = {},
}
META.__index = META

-- lua_run_cl PrintTable(swtf.m_AttributeManager.m_tList[2021])
function META:Initialize(data)
    swtf.LOG("attrib mngr init!!")
    --print(data, table.Count(data))

    swtf.KeyValuesForEach(data, function(k,v)
        local tab = {}

        tab.m_tKeyValues = v

        for k2,v2 in next, v do
            tab[v2.Key] = v2.Value
        end

        self.m_tList[tonumber(k)] = tab

        local classname = tab.attribute_class
        if not classname and tab.name then
            classname = tab.name:gsub(" ", "_") -- "cannot delete" == "cannot_delete"
        end

        if classname then
            self.m_tClassToID[classname] = tonumber(k)
        end

        --swtf.LOG("registered attribute %d %s", tonumber(k), classname)
    end)
end

function META:GetAttributeName(iAttributeID)
    if not isnumber(iAttributeID) then return "" end

    local tab = self.m_tList[iAttributeID]
    if not tab then return "" end

    return tab.attribute_class
end

function META:GetAttributeID(strAttributeName)
    local iAttributeID = self.m_tClassToID[strAttributeName]
    return iAttributeID
end

function META:LookupAttribute(iAttributeID)
    if not isnumber(iAttributeID) then return end
    return self.m_tList[iAttributeID]
end

function swtf.AttributeManager()
    return setmetatable({}, META)
end
