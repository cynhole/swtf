AddCSLuaFile()

local META = {
    m_tList = {},
    m_tListInheritted = {},
}
META.__index = META

function META:Initialize(data)
    swtf.LOG("prefab mngr init!!")

    swtf.KeyValuesForEach(data, function(strPrefabName, tab)
        local tPrefab = table.CollapseKeyValue(tab)

        tPrefab.m_tKeyValues = tab
        --if tPrefab.prefab then
        --    print(strPrefabName, "EXTENDS", tPrefab.prefab)
        --end

        --if strPrefabName == "weapon_smg" then
        --    PrintTable(tPrefab)
        --end

        self.m_tList[strPrefabName] = tPrefab
    end)
end

function META:GetPrefab(strPrefabName)
    if not self.m_tListInheritted[strPrefabName] then
        local tPrefab = self.m_tList[strPrefabName]
        if not tPrefab then return end

        local tRet = table.Copy(tPrefab)

        if tPrefab.prefab then
            local tPrefabs = string.Split(tPrefab.prefab, " ")

            for _, strPrefab in pairs(tPrefabs) do
                tBaseClass = self:GetPrefab(strPrefab)
                if not tBaseClass then continue end

                swtf.MergeInherit(tRet, tBaseClass)
            end
        end

        self.m_tListInheritted[strPrefabName] = tRet
    end

    return self.m_tListInheritted[strPrefabName]
end

function swtf.PrefabManager()
    return setmetatable({}, META)
end
