AddCSLuaFile()

TF_WEAPON_PRIMARY_MODE = 0
TF_WEAPON_SECONDARY_MODE = 1

local META = {
    m_tKeyValues = {},
    m_tKeyValuesCollapsed = {},

    m_tWeaponData = {},

    m_iWeaponType = 0,

    -- Grenade.
    m_bGrenade = false,
    m_flDamageRadius = 0,
    m_flPrimerTime = 0,
    m_bLowerWeapon = false,
    m_bSuppressGrenTimer = false,

    -- Skins
    m_bHasTeamSkins_Viewmodel = false,
    m_bHasTeamSkins_Worldmodel = false,

    -- Muzzle flash
    m_szMuzzleFlashModel = "",
    m_flMuzzleFlashModelDuration = 0,
    m_szMuzzleFlashParticleEffect = "",

    -- Tracer
    m_szTracerEffect = "",

    -- Eject Brass
    m_bDoInstantEjectBrass = true,
    m_szBrassModel = "",

    -- Explosion Effect
    m_szExplosionSound = "",
    m_szExplosionEffect = "",
    m_szExplosionPlayerEffect = "",
    m_szExplosionWaterEffect = "",

    m_bDontDrop = false,

    iMaxClip1 = -1,
    iMaxClip2 = -1,
    iDefaultClip1 = -1,
    iDefaultClip2 = -1,

    szViewModel = "",
}
META.__index = META

function META:Initialize(strData)
    if not isstring(strData) then return false end

    local data = util.KeyValuesToTable(strData, true, false)
    if not data then return false end

    self.m_tKeyValues = data
    self.m_tWeaponData = {}

    self.m_tWeaponData[TF_WEAPON_PRIMARY_MODE] = {}
    self.m_tWeaponData[TF_WEAPON_SECONDARY_MODE] = {}

    local PrimaryData = self.m_tWeaponData[TF_WEAPON_PRIMARY_MODE]
    local SecondaryData = self.m_tWeaponData[TF_WEAPON_SECONDARY_MODE]

    -- Primary fire mode.
    PrimaryData.m_nDamage = data.damage or 0
    PrimaryData.m_flRange = data.range or 8192
    PrimaryData.m_nBulletsPerShot = data.bulletspershot or 0
    PrimaryData.m_flSpread = data.spread or 0
    PrimaryData.m_flPunchAngle = data.punchangle or 0
    PrimaryData.m_flTimeFireDelay = data.timefiredelay or 0
    PrimaryData.m_flTimeIdle = data.timeidle or 0
    PrimaryData.m_flTimeIdleEmpty = data.timeidleempy or 0
    PrimaryData.m_flTimeReloadStart = data.timereloadstart or 0
    PrimaryData.m_flTimeReload = data.timereload or 0
    PrimaryData.m_bDrawCrosshair = (data.drawcrosshair or 1) > 0
    PrimaryData.m_iAmmoPerShot = data.ammopershot or 1
    PrimaryData.m_bUseRapidFireCrits = (data.userapidfirecrits or 0) ~= 0

    PrimaryData.m_iProjectile = 0
    local pszProjectileType = data.projectiletype or "projectile_none"

    for i, ProjName in next, swtf.ProjectileNames do
        if pszProjectileType == ProjName then
            PrimaryData.m_iProjectile = i
            break
        end
    end

    PrimaryData.m_flProjectileSpeed = data.projectilespeed or 0
    PrimaryData.m_flSmackDelay = data.smackdelay or 0.2
    SecondaryData.m_flSmackDelay = data.secondary_smackdelay or 0.2

    self.m_bDoInstantEjectBrass = (data.doinstantejectbrass or 1) ~= 0
    self.m_szBrassModel = data.brassmodel or ""

    -- Secondary fire mode.
    -- Inherit from primary fire mode
    SecondaryData.m_nDamage = data.secondary_damage or PrimaryData.m_nDamage
    SecondaryData.m_flRange = data.secondary_range or PrimaryData.m_flRange
    SecondaryData.m_nBulletsPerShot = data.secondary_bulletspershot or PrimaryData.m_nBulletsPerShot
    SecondaryData.m_flSpread = data.secondary_spread or PrimaryData.m_flSpread
    SecondaryData.m_flPunchAngle = data.secondary_punchangle or PrimaryData.m_flPunchAngle
    SecondaryData.m_flTimeFireDelay = data.secondary_timefiredelay or PrimaryData.m_flTimeFireDelay
    SecondaryData.m_flTimeIdle = data.secondary_timeidle or PrimaryData.m_flTimeIdle
    SecondaryData.m_flTimeIdleEmpty = data.secondary_timeidleempy or PrimaryData.m_flTimeIdleEmpty
    SecondaryData.m_flTimeReloadStart = data.secondary_timereloadstart or PrimaryData.m_flTimeReloadStart
    SecondaryData.m_flTimeReload = data.secondary_timereload or PrimaryData.m_flTimeReload
    SecondaryData.m_bDrawCrosshair = (data.secondary_drawcrosshair or (PrimaryData.m_bDrawCrosshair and 1 or 0)) > 0
    SecondaryData.m_iAmmoPerShot = data.secondary_ammopershot or PrimaryData.m_iAmmoPerShot
    SecondaryData.m_bUseRapidFireCrits = (data.secondary_userapidfirecrits or 0) ~= 0

    SecondaryData.m_iProjectile = PrimaryData.m_iProjectile
    pszProjectileType = data.secondary_projectiletype or "projectile_none"

    for i, ProjName in next, swtf.ProjectileNames do
        if pszProjectileType == ProjName then
            SecondaryData.m_iProjectile = i
            break
        end
    end

    self.iMaxClip1 = data.clip_size or -1 					            -- Max primary clips gun can hold (assume they don't use clips by default)
    self.iMaxClip2 = data.clip2_size or -1 					            -- Max secondary clips gun can hold (assume they don't use clips by default)
    self.iDefaultClip1 = data.default_clip or self.iMaxClip1 		    -- amount of primary ammo placed in the primary clip when it's picked up
    self.iDefaultClip2 = data.default_clip2 or self.iMaxClip2 		    -- amount of secondary ammo placed in the secondary clip when it's picked up

    self.szViewModel = data.viewmodel or ""
    --[[
    -- Primary ammo used
	const char *pAmmo = pKeyValuesData->GetString( "primary_ammo", "None" );
	if ( strcmp("None", pAmmo) == 0 )
		Q_strncpy( szAmmo1, "", sizeof( szAmmo1 ) );
	else
		Q_strncpy( szAmmo1, pAmmo, sizeof( szAmmo1 )  );
	iAmmoType = GetAmmoDef()->Index( szAmmo1 );
	
	-- Secondary ammo used
	pAmmo = pKeyValuesData->GetString( "secondary_ammo", "None" );
	if ( strcmp("None", pAmmo) == 0)
		Q_strncpy( szAmmo2, "", sizeof( szAmmo2 ) );
	else
		Q_strncpy( szAmmo2, pAmmo, sizeof( szAmmo2 )  );
	iAmmo2Type = GetAmmoDef()->Index( szAmmo2 );
    ]]

    -- MORE LATER

    return true
end


function swtf.WeaponInfo()
    return setmetatable({}, META)
end

