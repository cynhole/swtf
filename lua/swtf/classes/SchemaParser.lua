AddCSLuaFile()

AddCSLuaFile("json.lua")
local json = include("json.lua")

local LANGUAGE = "en_US"

local META = {
    m_strAPIKey = "",
    m_strRawSchemaData = "",
    m_strCompressedSchemaData = "",
    m_tSchemaData = {},
    m_strCRC = "",
    m_bParsed = false,
}
META.__index = META

function META:GetAPIKey()
    if self.m_strAPIKey == "" and file.Exists("cfg/swtf_api.cfg", "MOD") then
        self.m_strAPIKey = file.Read("cfg/swtf_api.cfg", "MOD")
    end

    return self.m_strAPIKey
end

function META:RequestSchema()
    local bBackupExists = file.Exists("swtf_items_game.dat", "DATA")

    if SERVER then
        local strKey = self:GetAPIKey()

        -- no key found
        if strKey == "" then
            if not bBackupExists then
                swtf.LOG("you're missing a steam API key! tf2 weapons will not load!")
                swtf.LOG("please put your steam API key in cfg/swtf_api.cfg")

                return
            else
                swtf.LOG("you're missing a steam API key! tf2 weapons will load, but with a potentially outdated item schema!")
            end
        end

        if not bBackupExists then
            swtf.LOG("initializing after requesting item schema from steam api!")

            http.Fetch(Format("http://api.steampowered.com/IEconItems_440/GetSchemaItems/v0001/?key=%s&language=%s", strKey, LANGUAGE), function(body, size, headers, code)
                local obj = json.decode(body)

                if not obj then
                    swtf.LOG("failed to decode json from steam api!!!!")
                    return
                end

                swtf.LOG("got items_game url %s", obj.result.items_game_url)

                http.Fetch(obj.result.items_game_url,
                    function(items_body, _, _, items_code)
                        swtf.LOG("GOT ITEM SCHEMA")

                        file.Write("swtf_items_game.dat", items_body)

                        self.m_strRawSchemaData = items_body
                        self:ParseSchema()
                    end,
                    function()
                        swtf.LOG("Steam API: requesting item schema failed")
                    end)
            end, function(error)
               swtf.LOG("Steam API: requesting items_game info failed")
            end)
        else
            swtf.LOG("initializing using backup items_game.dat")

            file.AsyncRead("swtf_items_game.dat", "DATA", function(_, _, status, strRawItemsKeyValues)
                if status ~= FSASYNC_OK then
                    swtf.LOG("failed to read data/items_game.dat!")
                    return
                end

                self.m_strRawSchemaData = strRawItemsKeyValues
                self:ParseSchema()
            end)
        end
    else
        net.Start("swtf_init")
            net.WriteUInt(swtf.enum.PARSER_CL_VERIFY, 4)

            net.WriteBool(bBackupExists) -- do we already have the schema?

            if bBackupExists then
                net.WriteString(util.CRC(file.Read("swtf_items_game.dat", "DATA"))) -- send CRC of file to see if server has different file
            end
        net.SendToServer()

        if not bBackupExists then
            swtf.LOG("requesting item schema from server...")
        else
            swtf.LOG("verifying local copy of item schema with server...")
        end
    end
end



--[[
    NOTE:

    prefab.item_class == entity class

    seems that prefabs point to what classname we need to spawn, and that each entity is available to use on each TF2 class
    so maybe try to figure out how i can spawn each weapon specifically for each class, and using the item def for its attributes
    weapons can share a classname, but have different item defs

    for example, sniper rifle reskins like stock sniper and the AWPer hand, or the Machina and the Shooting Star will share the same classnames
    but they are still distinct items from each other



    a more robust spawning/giving system is required to achieve this
]]


-- wep classname == itemdef.item_class
-- wep printname == itemdef.item_name
local function register_weapon(iItemDefIndex, pItemDef, strClass)
    local classname = pItemDef.item_class or pItemDef.name
    classname = classname:lower()
    classname = classname:gsub(" ", "_")
    classname = classname:gsub("%.", "")

    local PrefabData = {}
    if pItemDef.prefab then
        local tStrPrefabs = string.Split(pItemDef.prefab, " ")
        local tCoolPrefabs = {}

        for i, str in ipairs(tStrPrefabs) do
            tCoolPrefabs[str] = swtf.m_PrefabManager:GetPrefab(str)
        end

        for k, v in next, tCoolPrefabs do
            swtf.MergeInherit(PrefabData, v)
        end
    end

    local tUsedByClasses = table.Copy(pItemDef.used_by_classes or {})
    if PrefabData.used_by_classes then
        table.Merge(tUsedByClasses, PrefabData.used_by_classes)
    end

    -- we're used by multiple classes in tf2; so register for each class
    local iClasses = table.Count(tUsedByClasses)
    if not strClass then
        if iClasses > 1 then
            for class in next, tUsedByClasses do
                --swtf.LOG("register wep for multiclass %s %s", class, classname)
                register_weapon(iItemDefIndex, pItemDef, class)
            end

            return
        elseif iClasses == 1 then
            strClass = next(tUsedByClasses)
        end
    elseif iClasses > 1 then
        classname = Format("%s_%s", classname, strClass)
    end

    swtf.LOG("register %s (%s, %s) as class %s", pItemDef.name, classname, pItemDef.class or "NIL!?", strClass or "NONE?!")

    local pItemData = table.Copy(pItemDef)
    swtf.MergeInherit(pItemData, PrefabData)
    pItemData.BaseClass = nil

    local SWEP = {
        Primary = {}, Secondary = {},
        Spawnable = true,
        ClassName = classname,
        PrintName = pItemData.name,

        m_iItemDefIndex = iItemDefIndex,
        m_tItemDef = pItemDef,

        m_tItemData = pItemData,
        m_tPrefabData = PrefabData,

        ViewModels = {},
    }

    if isstring(pItemData.model_player) then
        SWEP.ViewModels[Model(pItemData.model_player)] = true
    end

    -- bot killer items
    if isstring(pItemData.extra_wearable_vm) then
        SWEP.ViewModels[Model(pItemData.extra_wearable_vm)] = true
    end

    -- festive items
    if istable(pItemData.visuals) and istable(pItemData.visuals.attached_models) then
        for _, tModel in next, pItemData.visuals.attached_models do
            if isstring(tModel.model) then
                SWEP.ViewModels[Model(tModel.model)] = true
            end
        end
    end

    SWEP.Base = Format("weapon_swtf%s%s", (strClass and "_" .. strClass) or "", "_base" )
    SWEP.Category = Format("TF2%s", strClass and " " .. strClass or "")

    --swtf.LOG("registered itemdef %s (%d, %s, %s)", SWEP.ClassName, iItemDefIndex, SWEP.Base, strClass)
    weapons.Register(SWEP, SWEP.ClassName)
end

--[[
    Normal Weapons
    The first 32 items listed describe the original class weapons, with an extraneous "TF_WEAPON_BUILDER" at defindex 28, which has the icon of the Engineer's Destroy PDA but the model of the Build PDA, and a "TF_WEAPON_FLAREGUN" at defindex 31, which uses "#TF_Weapon_Flaregun" for both item_name and item_type_name (where the normal "Unique Achievement Flaregun" uses "#TF_Unique_Achievement_Flaregun" for the item_name) and the pistol for the model. Every weapon in the first 32's name is its item_class in all caps.

    Both Engineer PDAs and the Spy PDA use "#TF_Weapon_PDA_Engineer" for both their item_type_name and their item_name.
]]

--[[
    homonovus notes:
        prefab is factories
        item is the weapon itself
        attributes are "functions" that the prefabs/items use so the game knows what to do

        item def indeces [0-30] are stock weapons
        item def indeces [8000-12011 are tournament medals
]]

swtf.m_tItems = {}
local items_ignore = {
    ["tf_wearable"] = true,
    ["bundle"] = true,
    ["craft_item"] = true,
    ["tool"] = true,
    ["supply_crate"] = true,
    ["class_token"] = true,
    ["slot_token"] = true,
}
local tab_init_funcs = {
    ["attributes"] = function(tab)
        swtf.m_AttributeManager:Initialize(tab)
    end,
    ["items"] = function(tab) -- this is where weapons are registered in lua
        local t = table.CollapseKeyValue(tab)

        for iItemDefIndex, data in next, t do
            iItemDefIndex = tonumber(iItemDefIndex)
            if not isnumber(iItemDefIndex) then continue end

            swtf.m_tItems[iItemDefIndex] = data

            if data.craft_class == "weapon" then
                --print(iItemDefIndex, data.name, data.prefab)
                register_weapon(iItemDefIndex, data)
            end
            --if not items_ignore[data.item_class] then
            --    register_weapon(iItemDefIndex, data)
            --end
        end

        swtf.LOG("registered all weapons :)")
    end,
    ["prefabs"] = function(tab)
        swtf.m_PrefabManager:Initialize(tab)
    end
}

function META:ParseSchema()
    swtf.LOG("parsing item schema!")

    if SERVER and self.m_strCompressedSchemaData == "" then
        self.m_strCompressedSchemaData = util.Compress(self.m_strRawSchemaData)
    end

    self.m_strCRC = util.CRC(self.m_strRawSchemaData)

    local dat = util.KeyValuesToTablePreserveOrder(self.m_strRawSchemaData, true, false)

    swtf.KeyValuesForEach(dat, function(k,v)
        if isfunction(tab_init_funcs[k]) then
            tab_init_funcs[k](v)
            return
        end

        --print(k,v)
        --if k == "prefabs" then
        --    PrintTable(v)
        --end
    end)

    self.m_bParsed = true

    self:OnFinishParsing()
end

function META:OnFinishParsing() end

function swtf.SchemaParser()
    return setmetatable({}, META)
end
