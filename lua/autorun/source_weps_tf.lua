_G.swtf = {}
local swtf = swtf

-- this is dumb, i hate it
function swtf.KeyValuesForEach(kvs, cb)
    for _, t in ipairs(kvs) do
        local bRet = cb(t.Key, t.Value)
        if bRet == false then break end
    end
end

-- modified table.Merge to also behave like table.Inherit
-- does not replace values that already exist
function swtf.MergeInherit(dest, source)
    for k, v in next, source do
        if istable(v) and istable(dest[k]) then
            -- don't overwrite one table with another
            -- instead merge them recurisvely
            swtf.MergeInherit(dest[k], v)
        elseif dest[k] == nil then
            dest[k] = v
        end
    end

    return dest
end

AddCSLuaFile("swtf/cl_init.lua")

include("swtf/enum.lua")
include("swtf/classes/AttributeManager.lua")
include("swtf/classes/PrefabManager.lua")
include("swtf/classes/SchemaParser.lua")
include("swtf/classes/WeaponInfo.lua")
include("swtf/classes/UniformRandomStream.lua")

-- id -> data
swtf.attributes = {}
swtf.prefabs    = {}

-- name -> id
swtf.attributesNameIDs = {}

function swtf.LookupAttribute(name)
    name = name:lower()

    local iAttributeID = swtf.attributesNameIDs[name]
    if not iAttributeID then return -1 end

    return iAttributeID
end

swtf.DEBUG = true
function swtf.LOG(fmt, ...)
    if not swtf.DEBUG then return end

    MsgC(SERVER and Color(0,128,255) or Color(255,196,64), "[swtf] ")
    print(Format(fmt, ...))
end

if CLIENT then
    include("swtf/cl_init.lua")
elseif SERVER then
    include("swtf/init.lua")
end
